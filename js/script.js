$(document).ready(function () {
  "use strict";
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  var gREADY_STATE_REQUEST_DONE = 4;
  var gSTATUS_REQUEST_DONE = 200;
  /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
  $("#btn-check").on("click", function () {
    console.log(
      "tôi lấy dữ liệu input có id là: " +
        $("#voucher").attr("id") +
        " và placeholder là: " +
        $("#voucher").attr("placeholder")
    );
    console.log(
      "Tác động vào có Id là: " +
        $("#div-result-check").attr("id") +
        " và innerHTML là: " +
        $("#div-result-check").html()
    );
    onBtnCheckVoucherClick();
  });

  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
  // Hàm này để xử lý sự kiện nút check voucher click
  function onBtnCheckVoucherClick() {
    "use strict";
    // B1: lấy giá trị nhập trên form, mã voucher (Thu thập dữ liệu)
    var vVoucherObj = {
      maGiamGia: "",
    };
    readData(vVoucherObj);

    // B2: Validate data
    var vIsValidateData = validateData(vVoucherObj);
    if (vIsValidateData) {
      // B3: Tạo request và gửi mã voucher về server
      var bXmlHttp = new XMLHttpRequest();
      sendVoucherToServer(vVoucherObj, bXmlHttp);
      // B4: xu ly response khi server trả về
      bXmlHttp.onreadystatechange = function () {
        if (
          bXmlHttp.readyState === gREADY_STATE_REQUEST_DONE &&
          bXmlHttp.status === gSTATUS_REQUEST_DONE
        ) {
          processResponse(bXmlHttp);
        }
      };
    }
  }

  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
  function readData(paramVoucher) {
    "use strict";
    var vInputVoucher = $("#voucher");
    var vValueVoucher = vInputVoucher.val();
    paramVoucher.maGiamGia = vValueVoucher;
  }
  // Hàm này dùng để validate data
  // input: ma voucher nguoi dung nhap vao
  // output: tra ve true neu ma voucher duoc nhap, nguoc lai thi return false
  function validateData(paramVoucher) {
    if (paramVoucher.maGiamGia === "") {
      // Hiển thị câu thông báo lỗi ra
      $("#div-result-check").html("Mã giảm giá chưa nhập!");
      // Thay đổi class css, để đổi màu chữ thành màu đỏ
      $("#div-result-check").prop("class", "text-danger");
      return false;
    }

    // Thay đổi class css, để đổi màu chữ thành màu đen bình thường
    $("div-result-check").prop("class", "text-dark");
    $("div-result-check").html("");
    return true;
  }

  // Ham thuc hien viec call api va gui ma voucher ve server
  function sendVoucherToServer(paramVoucher, paramXmlVoucherRequest) {
    paramXmlVoucherRequest.open(
      "GET",
      "http://203.171.20.210:8080/devcamp-pizza365/voucher_detail/" +
        paramVoucher.maGiamGia,
      true
    );
    paramXmlVoucherRequest.send();
  }

  // Hàm này được dùng để xử lý khi server trả về response
  function processResponse(paramXmlHttp) {
    "use strict";
    // B1: nhận lại response dạng JSON ở xmlHttp.responseText
    var vJsonVoucherResponse = paramXmlHttp.responseText;
    // B2: Parsing chuỗi JSON thành Object
    var vVoucherResObj = JSON.parse(vJsonVoucherResponse);
    console.log(vJsonVoucherResponse);
    // B3: xử lý mã giảm giá dựa vào đối tượng vừa có
    var vDiscount = vVoucherResObj.phanTramGiamGia;
    var vResultCheckElement = document.getElementById("div-result-check");
    //discount - -1 nghĩa là mã giảm giá không tồn tại
    if (vDiscount === "-1") {
      $("#div-result-check").html("Không tồn tại mã giảm giá");
    } else {
      $("#div-result-check").html("Mã giảm giá " + vDiscount + "%");
    }
  }
});
